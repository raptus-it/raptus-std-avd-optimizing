Clear-AzContext -Force
Login-AzAccount

#Basic Configuration
$rgname = "RG-WVD-AUTOSCALE"
$location = "West Europe"
$laworkspace = "AutoScaleLAWorkspace"

#Create minimal Ressources
New-AzResourceGroup -Name $rgname -Location $location
New-AzOperationalInsightsWorkspace -Location $location -Name $laworkspace -Sku Standard -ResourceGroupName $rgname

#Get Deployment Script
New-Item -ItemType Directory -Path "C:\Temp" -Force
Set-Location -Path "C:\Temp"
$Uri = "https://raw.githubusercontent.com/Azure/RDS-Templates/master/wvd-templates/wvd-scaling-script/CreateOrUpdateAzAutoAccount.ps1"
# Download the script
Invoke-WebRequest -Uri $Uri -OutFile ".\CreateOrUpdateAzAutoAccount.ps1"

#Configure Automation Account
$Params = @{
    "UseARMAPI"             = $true
    "ResourceGroupName"     = $rgname
    "Location"              = $location
    "WorkspaceName"         = $laworkspace
}

#Run Creation
.\CreateOrUpdateAzAutoAccount.ps1 @Params