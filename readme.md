# AVD Optimizing

## Autoscaling

Create an autoscale scaling plan according the [documentation](https://learn.microsoft.com/en-us/azure/virtual-desktop/autoscale-scaling-plan).  
It is important that you assign the "Desktop Virtualization Power On Off Contributor role" to Azure Virtual Desktop on the subscription level.
It is highly recommended that you provide an exception tag for maintenance purposes.
If you want to shut down all VMs in the evening, you have to set the value "Minimum percentage of hosts (%)" in the ramp down phase to zero.
You only need to set the schedule for the weekdays.
This way the weekend days adopt the settings of the ramp down phase.  
**Do not** enable "Force Logoff Users", unless the customer wants to enforce the shutdown of the virtual machine.

You must enable the host pool property ["start VM on connect"](#start-on-connect), if you want to use AVD outside the opening hours.  
The value "capacity threshold" of the scaling plan is dependant of the host pool property "max session limit".

You can ignore all the autoscaling scripts in this repository, if you use a scaling plan.

## Optimization

* **WICHTIG: WVD Optimized Settings** There is a Script in NinjaOne which runs the VDOT Optimizations, utilitzing the RVDOT/2009/ConfigurationFiles/AppxPackages.json settings. This script uses the [Github VDOT Script](https://github.com/The-Virtual-Desktop-Team/Virtual-Desktop-Optimization-Tool) Or use the script provided in NinjaRMM (rScript - Install-VDOT-Raptus)
* Use [AZURE PRIVATE ENDPOINTS](https://learn.microsoft.com/en-us/azure/storage/common/storage-private-endpoints) when providing profiles with FSLOGIX
* [RECOMMENDED: RDP Shortpath (used when VPN connection is available)](https://docs.microsoft.com/en-us/azure/virtual-desktop/shortpath) (03-avd-shortpath.ps1)

## Control and Test

* Test Shortpath - RDS connection must be UDP

## Apps and Languages

### M365 Apps for Business
Reconfigure the M365 Apps with several Languages (EN, DE, FR) and shared computer activation.

* Download the [Office Deployment Tool]("https://go.microsoft.com/fwlink/p/?LinkID=626065")
* Use the configuration XML (avd-multisession-office-default) in this repo for Multilanguage deployment for Shared Computers
```
.\setup.exe /download avd-multisession-office-default.xml
.\setup.exe /configure avd-multisession-office-default.xml
```

## Start On Connect

To let users start their VM on connect, by using the [Windows Desktop Client](https://docs.microsoft.com/en-us/windows-server/remote/remote-desktop-services/clients/windowsdesktop), it is important to follow this Microsoft Guideline:
[Start VM on Connect](https://learn.microsoft.com/en-gb/azure/virtual-desktop/start-virtual-machine-connect?tabs=azure-portal)

**Important reminder**: You have to add the Service Principal: Azure Virtual Desktop (in most case) or Windows Virtual Desktop and not the users or groups

### Security
**Important reminder**: [Assign Azure Role.](https://learn.microsoft.com/en-us/entra/identity/devices/howto-vm-sign-in-azure-ad-windows#azure-role-not-assigned) Verify that you've configured Azure RBAC policies for the VM that grant the user the Virtual Machine Administrator Login or Virtual Machine User Login role. Otherwise Users won't be able to login to the AVD.
