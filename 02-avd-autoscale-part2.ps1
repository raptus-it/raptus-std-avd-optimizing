Clear-AzContext -Force
Login-AzAccount

#Basic Configuration
$rgname = "RG-WVD-AUTOSCALE"
$laworkspace = "AutoScaleLAWorkspace"
$autoscaleaccount = "WVDAutoScaleAutomationAccount"
$azurerunasconnection = "AzureRunAsConnection"

#Get Deployment Script
New-Item -ItemType Directory -Path "C:\Temp" -Force
Set-Location -Path "C:\Temp"
$Uri = "https://raw.githubusercontent.com/Azure/RDS-Templates/master/wvd-templates/wvd-scaling-script/CreateOrUpdateAzLogicApp.ps1"
# Download the script
Invoke-WebRequest -Uri $Uri -OutFile ".\CreateOrUpdateAzLogicApp.ps1"

#Configuration
$ResourceGroup = Get-AzResourceGroup -Name $rgname
$WVDHostPool = Get-AzResource -ResourceType "Microsoft.DesktopVirtualization/hostpools" | Out-GridView -OutputMode:Single -Title "Select the host pool you'd like to scale"

$LogAnalyticsWorkspaceId = Get-AzOperationalInsightsWorkspace -Name $laworkspace -ResourceGroupName $rgname
$LogAnalyticsKey = Get-AzOperationalInsightsWorkspaceSharedKey -Name $laworkspace -ResourceGroupName $rgname

$AutoAccount = Get-AzAutomationAccount -Name $autoscaleaccount -ResourceGroupName $rgname
$AutoAccountConnection = Get-AzAutomationConnection -ResourceGroupName $AutoAccount.ResourceGroupName -AutomationAccountName $AutoAccount.AutomationAccountName -Name $azurerunasconnection

$WebhookURIAutoVar = Get-AzAutomationVariable -Name 'WebhookURIARMBased' -ResourceGroupName $AutoAccount.ResourceGroupName -AutomationAccountName $AutoAccount.AutomationAccountName

#Configure Automation Account
$Params = @{
     "ResourceGroupName"             = $ResourceGroup.ResourceGroupName
     "Location"                      = $ResourceGroup.Location
     "UseARMAPI"                     = $true
     "HostPoolName"                  = $WVDHostPool.Name
     "HostPoolResourceGroupName"     = $WVDHostPool.ResourceGroupName
     "LogAnalyticsWorkspaceId"       = $LogAnalyticsWorkspaceId
     "LogAnalyticsPrimaryKey"        = $LogAnalyticsKey.PrimarySharedKey
     "ConnectionAssetName"           = $AutoAccountConnection.Name
     "RecurrenceInterval"            = "15"
     "BeginPeakTime"                 = "08:00"
     "EndPeakTime"                   = "10:00"
     "TimeDifference"                = "+2:00"
     "SessionThresholdPerCPU"        = "2"
     "MinimumNumberOfRDSH"           = "0"
     "MaintenanceTagName"            = "maintenance"
     "LimitSecondsToForceLogOffUser" = "180"
     "LogOffMessageTitle"            = "Sie werden abgemeldet / Vous serez déconnecté"
     "LogOffMessageBody"             = "Sie werden in Kürze abgemeldet. Speichern Sie ihre Dokumente und beenden Sie alle Programme. / Vous serez bientôt déconnecté. Sauvegardez vos documents et quittez tous les programmes."
     "WebhookURI"                    = $WebhookURIAutoVar.Value
}

#Run Creation
.\CreateOrUpdateAzLogicApp.ps1 @Params